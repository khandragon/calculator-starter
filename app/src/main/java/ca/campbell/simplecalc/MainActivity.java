package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


//  TODO: add a clear button that will clear the result & input fields


public class MainActivity extends Activity {
    EditText etNumber1;
    EditText etNumber2;
    TextView result;
    double num1;
    double num2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    public void addNums(View v) {
        num1 = Double.parseDouble(etNumber1.getText().toString());
        num2 = Double.parseDouble(etNumber2.getText().toString());
        result.setText(Double.toString(num2+num1));
    }

    public void minusNums(View v) {
        num1 = Double.parseDouble(etNumber1.getText().toString());
        num2 = Double.parseDouble(etNumber2.getText().toString());
        result.setText(Double.toString(num1-num2));
    }
    public void divideNums(View v) {
        num1 = Double.parseDouble(etNumber1.getText().toString());
        num2 = Double.parseDouble(etNumber2.getText().toString());
        if(num2 != 0) {
            result.setText(Double.toString(num1 / num2));
        }else{
            result.setText("ERROR: Please input a valid number for denominatior");
        }
    }
    public void multiplyNums(View v) {
        num1 = Double.parseDouble(etNumber1.getText().toString());
        num2 = Double.parseDouble(etNumber2.getText().toString());
        result.setText(Double.toString(num2*num1));
    }
    }